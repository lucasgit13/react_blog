import { useState, useEffect } from "react";
import axios from "axios";

const useAxiosFetch = (dataUrl) => {
  const [data, setData] = useState([]);
  const [fetchError, setFetchError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    let isMounted = true;
    const source = axios.CancelToken.source();

    const fetchData = (url) => {
      setIsLoading(true);
      axios
        .get(url, {
          cancelToken: source.token,
        })
        .then((response) => {
          if (isMounted) {
            setData(response.data);
            setFetchError(null);
          }
        })
        .catch((err) => {
          if (isMounted) {
            setFetchError(err.message);
            setData([]);
          }
        })
        .finally(() => {
          isMounted && setIsLoading(false);
        });
    };
    fetchData(dataUrl);

    return () => {
      isMounted = false;
      source.cancel();
    };
  }, [dataUrl]);

  return { data, fetchError, isLoading };
};

export default useAxiosFetch;
