import React from 'react'
import { useParams } from 'react-router-dom'

const Test = () => {
  const {id} = useParams()
  return (
    <h1>Test file id: {id}</h1>
  )
}

export default Test